package ru.shutov.customerapp.constants;

public class Constants {
    public static final String CREATE_AGT = "CREATE-AGT";
    public static final String GET_BALANCE = "GET-BALANCE";
    public static final String REQUEST_TYPE = "request-type";
    public static final String RESPONSE = "response";
    public static final String RESULT_CODE = "result-code";
    public static final String NAME = "name";
    public static final String LOGIN = "login";
    public static final String PASSWORD = "password";
    public static final String EXTRA = "extra";
}
