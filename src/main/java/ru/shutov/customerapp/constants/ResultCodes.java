package ru.shutov.customerapp.constants;

public class ResultCodes {
    public static final int RESULT_CODE_0 = 0;
    public static final int RESULT_CODE_1 = 1;
    public static final int RESULT_CODE_2 = 2;
    public static final int RESULT_CODE_3 = 3;
    public static final int RESULT_CODE_4 = 4;
}
