package ru.shutov.customerapp.controllers;

import java.math.BigDecimal;

import javax.xml.bind.JAXBException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import ru.shutov.customerapp.constants.Constants;
import ru.shutov.customerapp.constants.ResultCodes;
import ru.shutov.customerapp.controllers.utils.XmlUtil;
import ru.shutov.customerapp.exceptions.CustomerExistsException;
import ru.shutov.customerapp.exceptions.CustomerNotFoundException;
import ru.shutov.customerapp.exceptions.WrongPasswordException;
import ru.shutov.customerapp.model.Extra;
import ru.shutov.customerapp.model.Response;
import ru.shutov.customerapp.service.CustomerService;

@Controller
@RequestMapping(value = "/customer")
public class CustomerController {

    @Autowired
    private CustomerService customerService;

    @RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_XML_VALUE, produces = MediaType.TEXT_XML_VALUE)
    public @ResponseBody String create(@RequestBody String request) throws JAXBException {
        String[] requestParameters = null;
        Response response = new Response();
        String resultString = null;
        try {
            requestParameters = XmlUtil.parse(request);
            
            String requestType = requestParameters[0];
            long login = Long.parseLong(requestParameters[1]);
            String password = requestParameters[2];
        
            if (requestType.equals(Constants.CREATE_AGT)) {
                customerService.create(login, password);
            }
        
            if (requestType.equals(Constants.GET_BALANCE)) {
                BigDecimal balance = customerService.getBalance(login, password);
                Extra extra = new Extra();
                extra.setBalance(balance);
                response.setExtra(extra);
            }
            
            response.setResultCode(ResultCodes.RESULT_CODE_0);
        } catch (CustomerExistsException e) {
            response.setResultCode(ResultCodes.RESULT_CODE_1);
        } catch (CustomerNotFoundException e) {
            response.setResultCode(ResultCodes.RESULT_CODE_3);
        } catch (WrongPasswordException e) {
            response.setResultCode(ResultCodes.RESULT_CODE_4);
        } catch (Exception e) {
            response.setResultCode(ResultCodes.RESULT_CODE_2);
        } finally {
            resultString = XmlUtil.marshal(response);
            return resultString;
        }
    }
}
