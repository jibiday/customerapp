package ru.shutov.customerapp.controllers.utils;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.StringWriter;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import ru.shutov.customerapp.constants.Constants;
import ru.shutov.customerapp.model.Response;

public class XmlUtil {
    public static String[] parse(String line) throws IOException, SAXException, ParserConfigurationException{
        String[] result = new String[3];
        DocumentBuilder documentBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
        Document document = documentBuilder.parse(new InputSource(new ByteArrayInputStream(line.getBytes("utf-8"))));

        Node requestNode = document.getDocumentElement();
        NodeList elements = requestNode.getChildNodes();
        for (int i = 0; i < elements.getLength(); i++) {
            Node element = elements.item(i);
            if (element.getNodeType() == Node.ELEMENT_NODE) {
                if (element.getNodeName().equals(Constants.REQUEST_TYPE)) {
                    result[0] = element.getChildNodes().item(0).getTextContent();
                }
                if (element.getNodeName().equals(Constants.EXTRA)) {
                    if (((Element)element).getAttribute(Constants.NAME).equals(Constants.LOGIN)) {
                        result[1] = element.getChildNodes().item(0).getTextContent();
                    }
                    if (((Element)element).getAttribute(Constants.NAME).equals(Constants.PASSWORD)) {
                        result[2] = element.getChildNodes().item(0).getTextContent();
                    }
                }
            }
        }
        return result;
    }
    
    public static String marshal(Response response) throws JAXBException {

        JAXBContext context = JAXBContext.newInstance(Response.class);
        Marshaller marshaller = context.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);

        StringWriter sw = new StringWriter();
        marshaller.marshal(response, sw);
        return sw.toString();
    }
}
