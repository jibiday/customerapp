package ru.shutov.customerapp.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import ru.shutov.customerapp.constants.Constants;

@XmlRootElement(name = Constants.RESPONSE)
@XmlType(propOrder={"resultCode", "extra"})
public class Response {
    private int resultCode;
    private Extra extra;

    @XmlElement(name = Constants.RESULT_CODE)
    public int getResultCode() {
        return resultCode;
    }
    public void setResultCode(int resultCode) {
        this.resultCode = resultCode;
    }
    @XmlElement(name = Constants.EXTRA)
    public Extra getExtra() {
        return extra;
    }
    public void setExtra(Extra extra) {
        this.extra = extra;
    }
}
