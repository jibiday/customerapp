package ru.shutov.customerapp.model;

import java.math.BigDecimal;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlValue;

@XmlRootElement(name = "extra")
public class Extra {
    private final String name = "balance";
    private BigDecimal balance;

    @XmlAttribute
    public String getName() {
        return name;
    }
    @XmlValue()
    public BigDecimal getBalance() {
        return balance;
    }
    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }
}
