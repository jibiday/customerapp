package ru.shutov.customerapp.exceptions;

public class CustomerExistsException extends Exception{
    private static final long serialVersionUID = 1L;

    public CustomerExistsException() {
        super();
    }

    public CustomerExistsException(String message) {
        super(message);
    }

    public CustomerExistsException(String message, Throwable cause) {
        super(message, cause);
    }
}
