package ru.shutov.customerapp.exceptions;

public class WrongPasswordException extends Exception{
    private static final long serialVersionUID = 1L;

    public WrongPasswordException() {
        super();
    }

    public WrongPasswordException(String message) {
        super(message);
    }

    public WrongPasswordException(String message, Throwable cause) {
        super(message, cause);
    }
}
