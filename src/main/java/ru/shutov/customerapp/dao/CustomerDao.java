package ru.shutov.customerapp.dao;

import java.math.BigDecimal;

import ru.shutov.customerapp.model.entities.Customer;

public interface CustomerDao {

    void create(Customer user) throws Exception;

    BigDecimal getBalance(long login, String password) throws Exception;
}
