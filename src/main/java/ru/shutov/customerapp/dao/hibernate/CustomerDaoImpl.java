package ru.shutov.customerapp.dao.hibernate;

import java.math.BigDecimal;
import java.util.List;

import javax.persistence.TypedQuery;

import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import ru.shutov.customerapp.dao.CustomerDao;
import ru.shutov.customerapp.dao.hibernate.utils.HibernateUtils;
import ru.shutov.customerapp.exceptions.CustomerExistsException;
import ru.shutov.customerapp.exceptions.CustomerNotFoundException;
import ru.shutov.customerapp.exceptions.WrongPasswordException;
import ru.shutov.customerapp.model.entities.Customer;

@Repository
public class CustomerDaoImpl implements CustomerDao {

    public void create(Customer customer) throws Exception {
        Session session = null;
        try {
            session = HibernateUtils.getSession();
            session.beginTransaction();
            Customer checkCustomer = getByLogin(customer.getLogin());
            if (checkCustomer == null) {
                session.save(customer);
            } else {
                throw new CustomerExistsException();
            }
            session.getTransaction().commit();
        } catch (Exception e) {
            if (session.getTransaction() != null) {
                session.getTransaction().rollback();
            }
            e.printStackTrace();
            throw e;
        } finally {
            if ((session != null) && session.isOpen()) {
                session.close();
            }
        }
    }

    public BigDecimal getBalance(long login, String password) throws Exception {
        Session session = null;
        Customer customer = null;
        try {
            session = HibernateUtils.getSession();
            customer = getByLogin(login);
            if (customer == null) {
                throw new CustomerNotFoundException();
            } else if (!customer.getPassword().equals(password)) {
                throw new WrongPasswordException();
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        } finally {
            if ((session != null) && session.isOpen()) {
                session.close();
            }
        }
        return customer.getBalance();
    }
    
    private Customer getByLogin(long login) {
        Session session = null;
        session = HibernateUtils.getSession();
        TypedQuery<Customer> query = session.createQuery("from Customer where login = :login");
        query.setParameter("login", login);
        List<Customer>  customerList = (List<Customer>) query.getResultList();
        if (customerList.size() > 0) {
            return customerList.get(0);
        } else {
            return null;
        }
    }
}
