package ru.shutov.customerapp.service;

import java.math.BigDecimal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ru.shutov.customerapp.dao.CustomerDao;
import ru.shutov.customerapp.model.entities.Customer;

@Service
public class CustomerServiceImpl implements CustomerService{

    @Autowired
    private CustomerDao customerDao;

    public CustomerServiceImpl(CustomerDao customerDao) {
        this.customerDao = customerDao;
    }

    public CustomerServiceImpl() {
    }

    public void create(long login, String password) throws Exception {
        Customer customer = new Customer();
        customer.setLogin(login);
        customer.setPassword(password);
        customer.setBalance(new BigDecimal(0));
        customerDao.create(customer);
    }

    public BigDecimal getBalance(long login, String password) throws Exception {
        return customerDao.getBalance(login, password);
    }
}
