package ru.shutov.customerapp.service;

import java.math.BigDecimal;

public interface CustomerService {
    
    void create(long login, String password) throws Exception;
    
    BigDecimal getBalance(long login, String password) throws Exception;
}
